-- Shorten function name
local keymap = vim.keymap.set
-- Silent keymap option
local opts = { silent = true }

--Remap space as leader key
keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "

-- Modes
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",

-- Normal --
-- Better window navigation
keymap("n", "<C-h>", "<C-w>h", opts)
keymap("n", "<C-j>", "<C-w>j", opts)
keymap("n", "<C-k>", "<C-w>k", opts)
keymap("n", "<C-l>", "<C-w>l", opts)

-- nvim-buffermap
keymap("n", "<leader>tl", ":BufferList<CR>")
keymap("n", "<leader>tr", ":BufferRemap")
keymap("n", "<leader>ts", ":BufferSwitch")

-- PHP
keymap("n", "<leader>pl", ":!php -l %<CR>", opts)
keymap("n", "<leader>d", "odie();<esc>", opts)
keymap("n", "<leader>de", "odie('end');<esc>", opts)
keymap("n", "<leader>dh", "odie('here');<esc>", opts)

-- RST headings
-- Place a heading row below the current line
keymap("n", "<leader>h1", "yypv$<cmd>s/./#/g<CR><cmd>nohlsearch<CR><esc>o<CR>", opts)
keymap("n", "<leader>h2", "yypv$<cmd>s/./*/g<CR><cmd>nohlsearch<CR><esc>o<CR>", opts)
keymap("n", "<leader>h3", "yypv$<cmd>s/./=/g<CR><cmd>nohlsearch<CR><esc>o<CR>", opts)
keymap("n", "<leader>h4", "yypv$<cmd>s/./-/g<CR><cmd>nohlsearch<CR><esc>o<CR>", opts)
keymap("n", "<leader>h5", "yypv$<cmd>s/./^/g<CR><cmd>nohlsearch<CR><esc>o<CR>", opts)
keymap("n", "<leader>h6", "yypv$<cmd>s/./\"/g<CR><cmd>nohlsearch<CR><esc>o<CR>", opts)

-- Less problematic search/replace
keymap("v", "<leader>s", ":s/")
keymap("n", "<leader>gs", ":%s/")

-- Clear highlights
keymap("n", "<leader>h", "<cmd>nohlsearch<CR>", opts)

-- Close buffers
keymap("n", "<S-q>", "<cmd>Bdelete!<CR>", opts)

-- Better paste
keymap("v", "p", '"_dP', opts)

-- Visual --
-- Stay in indent mode
keymap("v", "<", "<gv", opts)
keymap("v", ">", ">gv", opts)

-- Plugins --

-- NvimTree
keymap("n", "<leader>e", ":NvimTreeToggle<CR>", opts)

-- Telescope
keymap("n", "<leader>ff", ":Telescope find_files<CR>", opts)
keymap("n", "<leader>ft", ":Telescope live_grep<CR>", opts)
keymap("n", "<leader>fp", ":Telescope projects<CR>", opts)
keymap("n", "<leader>fb", ":Telescope buffers<CR>", opts)

-- Git
keymap("n", "<leader>gg", "<cmd>lua _LAZYGIT_TOGGLE()<CR>", opts)

-- Comment
keymap("n", "<leader>/", "<cmd>lua require('Comment.api').toggle_current_linewise()<CR>", opts)
keymap("x", "<leader>/", '<ESC><CMD>lua require("Comment.api").toggle_linewise_op(vim.fn.visualmode())<CR>')
